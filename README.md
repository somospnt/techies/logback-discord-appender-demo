## Logback con appender a Discord

Proyecto de ejemplo que utiliza logback para enviar un mensaje a un canal de Discord. 

* El canal de Discord tiene asociado un webhook.
* La configuración del appender está en el archivo logback.xml 

Más info sobre el appender en https://github.com/napstr/logback-discord-appender
