package com.somospnt.discord;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App {

    private final static Logger logger = LoggerFactory.getLogger(App.class);

    /**
     * Utiliza logback para enviar un mensaje a un canal de Discord. 
     * El canal de Discord tiene asociado un webhook. 
     * La configuración del appender está en el archivo logback.xml
     * 
     * Más info sobre el appender en https://github.com/napstr/logback-discord-appender
     */
    public static void main(String[] args) {
        logger.info("Msg de info");
        logger.warn("Msg de warning");
        logger.error("Msg de error a Discord también!");
        logger.debug("Msg de debug");
    }
}
